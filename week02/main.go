package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

func main()  {
	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		status := http.StatusOK

		// 1.接收客户端 request，并将 request 中带的 header 写入 response header
		for key, values := range request.Header {
			for _, value := range values {
				writer.Header().Add(key, value)
			}
		}
		// 2.读取当前系统的环境变量中的 VERSION 配置，并写入 response header
		writer.Header().Set("VERSION", os.Getenv("VERSION"))

		// 3.Server 端记录访问日志包括客户端 IP，HTTP 返回码，输出到 server 端的标准输出
		fmt.Println("status:", status)
		fmt.Println("client ip:", request.RemoteAddr)

		writer.WriteHeader(status)
		fmt.Fprintf(writer, "hello world!")
	})

	// 4.当访问 localhost/healthz 时，应返回 200
	http.HandleFunc("/healthz", func(writer http.ResponseWriter, request *http.Request) {
		writer.WriteHeader(http.StatusOK)
	})

	log.Fatal(http.ListenAndServe(":3000", nil))
}